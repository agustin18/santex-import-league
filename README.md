# Santex Import League

The goal is to make a project that exposes an API with an HTTP GET in this URI: /import-league/{leagueCode} . E.g., it must be possible to invoke the service using this URL:
http://localhost:<port>/import-league/CL

## How to run the project

### Docker Compose

    The project already creates the DB structure automatically

    1. Build and run the image (docker-compose up --build)
    2. If you like to change the ports (default is 8008) you can change it in the docker-compose.yaml file
    3. Run tests (docker-compose exec backend pytest --cov)

## Project Overview

The service implementation must get data using the given {leagueCode}, by making requests to the http://www.football-data.org/ API (you can see the documentation entering to the site, use the API v2),  and import the data into a DB (MySQL is suggested, but you can use any DB of your preference). The data requested is:

Competition ("name", "code", "areaName")

Team ("name", "tla", "shortName", "areaName", "email")

Player("name", "position", "dateOfBirth", "countryOfBirth", "nationality")


Feel free to add to this data structure any other field that you might need (for the foreign keys relationship).


Additionally, expose an HTTP GET in URI /total-players/{leagueCode}  , with a simple JSON response like this:
{"total" : N } and HTTP Code 200.

where N is the total amount of players belonging to all teams that participate in the given league (leagueCode). This service must rely exclusively on the data saved inside the DB (it must not access the API football-data.org). If the given leagueCode is not present into the DB, it should respond an HTTP Code 404.


Once you have finished the project, you must upload all the relevant files inside a ZIP compressed file. It must include all the sources, plus the files related to project configuration and/or dependency management.

### Data Sources

The data source for this API:
1. MySQL 8 database
2. http://www.football-data.org/v2 APIs particulary:
    * competitions/{competition_code}/teams (To get Competition and Team Data)
    * teams/{team_id}/ (To get Players Data)


### API Design and Technical Decisions

Python and Django have been my main language/framework for more than 4 years. I think designing and build an API with DRF allows you to do it easily and fast. On the other hand, the Django ORM enables you to interact with your database (MySQL) without complexity. Pytest was chosen for the familiarity I have with it, and the advantage of its fixtures and parametrize.

Docker with docker-compose was chosen because is easy to create, deploy, and run applications by using containers

With requests and requests-toolbelt I created the retry policy for the main issue the application has, it's been elaborated and explained in the `api.connection.py` but the import of a competition with a lot of teams like the Champions League(more than 70 teams) could last 8 minutes to get all the information


### Notes and Acceptance Criteria

- [x] You are allowed to use any library related to the language in which you are implementing the project.

- [x] You must provide the SQL for data structure creation; it is a plus that the project automatically creates the structure (if it doesn't exist) when it runs the first time.

- [x] All the mentioned DB entities must keep their proper relationships (the players with which team they belong to; the teams in which leagues participate).

- [x] It might happen that when a given leagueCode is being imported, the league has participant teams that are already imported (because each team might belong to one or more leagues). For these cases, it must add the relationship between the league and the team(s) (and omit the process of the preexistent teams and their players).

- [x] The API responses for /import-league/{leagueCode} are:
 HttpCode 201, {"message": "Successfully imported"} --> When the leagueCode was successfully imported.
 HttpCode 409, {"message": "League already imported"} --> If the given leagueCode was already imported into the DB (and in this case, it doesn't need to be imported again).
 HttpCode 404, {"message": "Not found" } --> if the leagueCode was not found.
 HttpCode 504, {"message": "Server Error" } --> If there is any connectivity issue either with the football API or the DB server.

## Testing

In order to not use our actual sources as we are limited (I elaborate more about it in the `api.connection.py`, in the testing, I mocked the 3rd party responses with pytest fixtures and test all the code has been written in this API (with a 100% test coverage)


### API Documentation

In addition, I added the first version of API documentation, with Swagger. You can access the path `/swagger`
