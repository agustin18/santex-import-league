from django.urls import (
    include,
    path,
)

from api.views import (
    GetCompetition,
    GetTotalPlayers,
)

from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Agustin Saiz Code Challenge')


santex_patterns = [
    path('import-league/<str:competition_code>', GetCompetition.as_view()),
    path('total-players/<str:competition_code>', GetTotalPlayers.as_view()),
    path('swagger/', schema_view),
]

urlpatterns = [
    path('', include(santex_patterns)),
]
