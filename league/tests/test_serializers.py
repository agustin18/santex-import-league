import pytest

from api.serializers import TeamSerializer

class TestSerializers:

    # as is the unique custon function about serializers should be good to test it
    @pytest.mark.django_db
    def test_team_creation_succesful(self, competition_1, team_validated_data):
        serializer = TeamSerializer(team_validated_data)
        created_team = serializer.create(team_validated_data, competition_1)
        assert created_team.competitions.values()[0]['id'] == competition_1.id

    @pytest.mark.django_db
    def test_team_creation_fail_wrong_competition(self, team_1, team_validated_data):
        serializer = TeamSerializer(team_validated_data)
        with pytest.raises(TypeError) as error:
            # with another type of model
            serializer.create(team_validated_data, team_1)
        assert "'Competition' instance expected" in str(error.value)
