import pytest
from unittest.mock import patch

from api.utils import (
    get_competition_data,
    get_players,
    get_teams_to_expand,
)


@pytest.mark.django_db
class TestUtils:

    def test_get_teams_to_expand(self, competition_with_teams_response, competition_1):
        response = get_teams_to_expand(competition_with_teams_response())
        assert response == [team['id'] for team in competition_with_teams_response()['teams']]

    def test_get_teams_to_expand_no_results(self, competition_with_teams_response, competition_1, team_1, team_2):
        response = get_teams_to_expand(competition_with_teams_response())
        # as the team is already in our database, we shouldn't retrieve it
        assert response == []

    @pytest.mark.parametrize("expected_status", [504, 404])
    def test_get_players_errors(
        self,
        requests_mock,
        team_1_squad_path,
        competition_with_teams_response,
        expected_status,
    ):
        ids_to_find = [team['id'] for team in competition_with_teams_response()['teams']]
        mock_request = requests_mock.get(team_1_squad_path, status_code=expected_status)
        response = get_players(ids_to_find)
        assert mock_request.called
        assert response == expected_status

    def test_get_players_success(
        self,
        requests_mock,
        team_1_squad_path,
        team_2_squad_path,
        competition_with_teams_response,
        team_1_with_squad_response,
        team_2_with_squad_response
    ):
        ids_to_find = [team['id'] for team in competition_with_teams_response()['teams']]
        mock_request = requests_mock.get(team_1_squad_path, status_code=200, json=team_1_with_squad_response)
        mock_request_2 = requests_mock.get(team_2_squad_path, status_code=200, json=team_2_with_squad_response)
        response = get_players(ids_to_find)
        assert mock_request.called
        assert mock_request_2.called
        assert response == 201

    @pytest.mark.parametrize("expected_status", [504, 404])
    def test_get_competiton_data_errors(
        self,
        requests_mock,
        competition_team_path,
        expected_status,
    ):
        mock_request = requests_mock.get(competition_team_path(), status_code=expected_status)
        response = get_competition_data('MTL')
        assert mock_request.called
        assert response == expected_status

    @patch('api.utils.get_teams_to_expand')
    @patch('api.utils.get_players')
    def test_get_competiton_data_success(
        self,
        mocked_get_players,
        mocked_get_teams_to_expand,
        requests_mock,
        competition_team_path,
        competition_with_teams_response,
    ):
        mock_request = requests_mock.get(
            competition_team_path(), status_code=200, json=competition_with_teams_response(2)
        )
        mocked_get_teams_to_expand.return_value = [5]
        mocked_get_players.return_value = 201
        response = get_competition_data('MTL')
        assert mock_request.called
        assert response == 201
