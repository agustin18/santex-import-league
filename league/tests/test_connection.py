import pytest
from requests.exceptions import ConnectionError

from api.connection import process_external_request


class TestConnection:

    def test_connection_refuse(self, connection_path):
        with pytest.raises(ConnectionError) as error:
            # with one retry to test it
            process_external_request(connection_path, 1, 1)
        assert 'Max retries exceeded with url' in str(error.value)

    def test_successful_connection(self, requests_mock, connection_path):
        mock_request = requests_mock.get(connection_path, json={'response': 'good_call'})
        response = process_external_request(connection_path, 1, 1)
        assert mock_request.called
        response.status_code = 200
