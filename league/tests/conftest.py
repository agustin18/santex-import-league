import pytest


@pytest.fixture
def api_client():
    from rest_framework.test import APIClient
    return APIClient()


@pytest.fixture
def connection_path():
    return 'http://localhost:9999'


@pytest.fixture
def competition_team_path():

    def _get_response(code='MTL'):
        return 'http://api.football-data.org/v2/competitions/{}/teams'.format(code)
    return _get_response


@pytest.fixture
def team_1_squad_path(team_1):
    return 'http://api.football-data.org/v2/teams/{}'.format(team_1.id)


@pytest.fixture
def team_2_squad_path(team_2):
    return 'http://api.football-data.org/v2/teams/{}'.format(team_2.id)


@pytest.fixture
def team_validated_data():
    return(
        {
            'id': 668,
            'name': 'Boca Juniors',
            'tla': 'BOC',
            'short_name': 'Boca',
            'area': 'Argentina',
            'email': 'info@cabj.com.ar'
        }
    )


@pytest.fixture()
def competition_with_teams_response():

    def _get_response(pk=1):
        return(
            {
                "competition": {
                    "id": pk,
                    "area": {
                        "id": 2077,
                        "name": "Europe"
                    },
                    "name": "European Championship",
                    "code": "EC",
                },
                "season": {},
                "teams": [
                    {
                        "id": 1,
                        "area": {
                            "id": 2088,
                            "name": "Germany"
                        },
                        "name": "Germany",
                        "shortName": "Germany",
                        "tla": "GER",
                        "email": "info@dfb.de",
                    },
                    {
                        "id": 2,
                        "area": {
                            "id": 2028,
                            "name": "Italy"
                        },
                        "name": "Italy",
                        "shortName": "Italy",
                        "tla": "ITA",
                        "email": "info@cit.it",
                    },
                ]
            }
        )
    return _get_response


@pytest.fixture()
def team_1_with_squad_response():
    return(
        {
            "id": 1,
            "area": {
                "id": 2088,
                "name": "Germany"
            },
            "activeCompetitions": [],
            "name": "Germany",
            "shortName": "Germany",
            "tla": "GER",
            "email": "info@dfb.de",
            "squad": [
                {
                    "id": 1,
                    "name": "Joël Drommel",
                    "position": "Goalkeeper",
                    "dateOfBirth": "1996-11-16T00:00:00Z",
                    "countryOfBirth": "Germany",
                    "nationality": "Germany",
                },
                {
                    "id": 2,
                    "name": "Joële Drommelo",
                    "position": "Goalkeepere",
                    "dateOfBirth": "1996-11-15T00:00:00Z",
                    "countryOfBirth": "Germany",
                    "nationality": "Germany",
                },
            ],
        }
    )


@pytest.fixture()
def team_2_with_squad_response():
    return(
        {
            "id": 2,
            "area": {
                "id": 2028,
                "name": "Italy"
            },
            "activeCompetitions": [],
            "name": "Italy",
            "shortName": "Italy",
            "tla": "ITA",
            "email": "info@cit.it",
            "squad": [
                {
                    "id": 3,
                    "name": "Alessandro Del Piero",
                    "position": "Striker",
                    "dateOfBirth": "1956-11-16T00:00:00Z",
                    "countryOfBirth": "Italy",
                    "nationality": "Italy",
                },
                {
                    "id": 4,
                    "name": "Fran Totti",
                    "position": "Defender",
                    "dateOfBirth": "1986-11-15T00:00:00Z",
                    "countryOfBirth": "Italy",
                    "nationality": "Italy",
                },
            ]
        }
    )


@pytest.fixture()
def competition_1():
    from api.models import Competition
    return Competition.objects.create(
        id=1,
        name='Test League',
        code='MTL',
        area='Test Area',
    )


@pytest.fixture()
def team_1(competition_1):
    from api.models import Team
    team_1 = Team.objects.create(
        id=1,
        name='Test Team 1',
        short_name='Team 1',
        tla='TTO',
        area=True,
        email='teamone@gmail.com',
    )
    team_1.competitions.add(competition_1)
    return team_1


@pytest.fixture()
def team_2(competition_1):
    from api.models import Team
    team_2 = Team.objects.create(
        id=2,
        name='Test Team 2',
        short_name='Team 2',
        tla='TTT',
        area=True,
        email='teamtwo@gmail.com',
    )
    team_2.competitions.add(competition_1)
    return team_2


@pytest.fixture()
def player_1(team_1):
    from api.models import Player
    return Player.objects.create(
        name='Agustin',
        position='Goalkeeper',
        date_of_birth='1956-11-16T00:00:00Z',
        country_of_birth='Italy',
        nationality='Argentina',
        team_id=team_1,
    )


@pytest.fixture()
def player_2(team_1):
    from api.models import Player
    return Player.objects.create(
        name='Fabio',
        position='Centre',
        date_of_birth='1986-11-16T00:00:00Z',
        country_of_birth='Italy',
        nationality='Brazil',
        team_id=team_1,
    )


@pytest.fixture()
def player_3(team_2):
    from api.models import Player
    return Player.objects.create(
        name='Steve',
        position='Defender',
        date_of_birth='1956-11-16T00:00:00Z',
        country_of_birth='Italy',
        nationality='United States',
        team_id=team_2,
    )


@pytest.fixture()
def player_4(team_2):
    from api.models import Player
    return Player.objects.create(
        name='Jerome',
        position='Extreme',
        date_of_birth='1956-11-16T00:00:00Z',
        country_of_birth='Italy',
        nationality='France',
        team_id=team_2,
    )
