import pytest


@pytest.mark.django_db
class TestGetTotalPlayers:
    def test_get_total_players_league(self, api_client, player_1, player_2, player_3, player_4, competition_1):
        response = api_client.get('/total-players/{}'.format(competition_1.code))
        assert response.status_code == 200
        assert response.data == {'total': 4}

    def test_get_total_players_league_not_found(self, api_client):
        response = api_client.get('/total-players/10')
        assert response.status_code == 404
        assert 'Not found' in str(response.data)


@pytest.mark.django_db
class TestGetCompetition:
    def test_get_competition_exists(self, api_client, competition_1):
        response = api_client.get('/import-league/{}'.format(competition_1.code))
        assert response.status_code == 409
        assert 'League already imported' in str(response.data)

    def test_get_total_players_league_integration(
        self,
        api_client,
        requests_mock,
        competition_team_path,
        team_1_squad_path,
        team_1_with_squad_response,
        team_2_squad_path,
        team_2_with_squad_response,
        competition_with_teams_response,
    ):
        competition = competition_with_teams_response(3)
        requests_mock.get(
            competition_team_path(competition['competition']['code']), status_code=200, json=competition
        )
        requests_mock.get(
            team_1_squad_path, status_code=200, json=team_1_with_squad_response
        )
        requests_mock.get(
            team_2_squad_path, status_code=200, json=team_2_with_squad_response
        )
        response = api_client.get('/import-league/{}'.format(competition['competition']['code']))
        assert response.status_code == 201
        assert 'Successfully imported' in str(response.data)
