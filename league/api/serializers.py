from rest_framework import serializers
from api.models import (
    Competition,
    Player,
    Team,
)


class CompetitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Competition
        fields = [
            'id',
            'name',
            'code',
            'area',
        ]

class TeamSerializer(serializers.ModelSerializer):

    class Meta:
        model = Team
        fields = [
            'id',
            'name',
            'tla',
            'short_name',
            'area',
            'email',
        ]

    # Added the competition relation to the object once is created
    def create(self, validated_data, competition):
        obj = Team.objects.create(**validated_data)
        obj.competitions.add(competition)
        return obj


class PlayerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Player
        fields = [
            'name',
            'position',
            'date_of_birth',
            'country_of_birth',
            'nationality',
            'team_id',
        ]
