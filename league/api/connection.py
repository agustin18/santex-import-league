from django.conf import settings

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from requests_toolbelt import sessions

headers = {'X-Auth-Token': settings.FOOTBALL_API_TOKEN}
base_url = settings.FOOTBALL_API_BASE_URL


def get_retry_strategy(total, backoff_factor):
    retry_strategy = Retry(
        total=total,
        backoff_factor=backoff_factor,
        status_forcelist=[429, 500, 502, 503, 504],
        method_whitelist=["HEAD", "GET", "OPTIONS"]
    )

    return HTTPAdapter(max_retries=retry_strategy)


def process_external_request(path, total=8, backoff_factor=1):
    """
    Here is the main limitation the API has at the moment. api.football-data only allows you to do
    10 requests per minute to their API, as is our core resource this is a big limitation for us. So, I
    Evaluated 3 options, one is to create a pool of API tokens, in order to have more attempts per minute,
    the second one is to use celery to run the process async and the third one and the one I've chosen
    is to create a retry strategy. I think is the best one, because the first one is to hack the system,
    the second one probably allows you to queue more requests and eventually to break the API. But in a second
    iteration probably I would try to implement a combination of the second and third ones.
    """
    adapter = get_retry_strategy(total, backoff_factor)
    http = sessions.BaseUrlSession(base_url=base_url)
    http.mount("https://", adapter)
    http.mount("http://", adapter)
    http.headers.update(headers)
    return http.get(path)
