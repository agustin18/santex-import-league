from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_409_CONFLICT,
    HTTP_504_GATEWAY_TIMEOUT,
)
from rest_framework.views import APIView

from api.models import (
    Competition,
    Player,
    Team,
)
from api.utils import get_competition_data

MESSAGES = {
    HTTP_201_CREATED: "Successfully imported",
    HTTP_400_BAD_REQUEST: "Bad request",
    HTTP_404_NOT_FOUND: "Not found",
    HTTP_409_CONFLICT: "League already imported",
    HTTP_504_GATEWAY_TIMEOUT: "Server Error",
}


class GetCompetition(APIView):
    """
    Given a competition code, the API check if it's exists in our DB and if it's not we hit
    the 3rd party API to get the competition, team and player data
    """
    def get(self, request, competition_code, format=None):
        try:
            Competition.objects.get(code=competition_code)
            return Response({'message': MESSAGES[HTTP_409_CONFLICT]}, status=HTTP_409_CONFLICT)
        except Competition.DoesNotExist:
            """
            I separated the logic of the process to another module to allow in the future
            the reuse of these functions if they are necessary and try to respect DRY. In this view, we
            hit our DB in order to check if the competition is there, if it's not we initiate the process
            to get the data from the external API. Where we return a brief message about the results.
            """
            response = get_competition_data(competition_code)
            return Response({'message': MESSAGES[response]}, status=response)


class GetTotalPlayers(APIView):
    """
    This API retrieves the amount of players for the given competition
    """

    def get(self, request, competition_code, format=None):
        try:
            competition = Competition.objects.get(code=competition_code)
            team_ids = Team.objects.filter(competitions__id=competition.id).values_list('id', flat=True)
            total_players = Player.objects.filter(team_id__in=team_ids).count()
            return Response({'total': total_players}, status=HTTP_200_OK)
        except Competition.DoesNotExist:
            response = HTTP_404_NOT_FOUND
            return Response({'message': MESSAGES[response]}, status=response)
