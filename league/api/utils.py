import requests

from rest_framework.status import (
    HTTP_201_CREATED,
    HTTP_404_NOT_FOUND,
    HTTP_504_GATEWAY_TIMEOUT,
)

from api.models import Competition, Team
from api.connection import process_external_request
from api.serializers import (
    CompetitionSerializer,
    PlayerSerializer,
    TeamSerializer,
)


def get_competition_data(competition_code):
    # Hit the API to get the teams and competition to process
    competition_teams_path = 'competitions/{}/teams'.format(competition_code)
    external_request = process_external_request(competition_teams_path)

    if external_request.status_code == requests.codes.ok:
        competition_teams_data = external_request.json()
        # Override area, that is located under ['area']['name']
        competition_data = competition_teams_data['competition']
        competition_data['area'] = competition_data['area']['name']
        serializer = CompetitionSerializer(data=competition_data)
        serializer.is_valid(serializer)
        serializer.save()
    elif external_request.status_code == 404:
        return HTTP_404_NOT_FOUND
    else:
        # With the retry strategy is pretty unlikey but should be handle
        return HTTP_504_GATEWAY_TIMEOUT
    response = HTTP_201_CREATED
    team_ids_to_expand = get_teams_to_expand(competition_teams_data)
    # if there are teams to expand their players, call the function below
    if team_ids_to_expand:
        response = get_players(team_ids_to_expand)
    return response


def get_teams_to_expand(teams_data):
    competition = Competition.objects.get(id=teams_data['competition']['id'])
    # Create a List to save the ids of the teams we should consult later to get their players
    teams_to_search_players = []
    for team in teams_data['teams']:
        try:
            db_team = Team.objects.get(id=team['id'])
            # If the team exists in our DB, we only add the relation with the competition
            db_team.competitions.add(competition)
        except Team.DoesNotExist:
            # Override area and convert shortName to snake case
            team['area'] = team['area']['name']
            team['short_name'] = team['shortName']
            serializer = TeamSerializer(data=team)
            serializer.is_valid(serializer)
            serializer.create(serializer.validated_data, competition)
            teams_to_search_players.append(team['id'])
    return teams_to_search_players


def get_players(team_ids_to_expand):
    for team_id in team_ids_to_expand:
        # Hit the API per team to get their players
        players_team_path = 'teams/{}'.format(team_id)
        external_request = process_external_request(players_team_path)
        if external_request.status_code == requests.codes.ok:
            data = external_request.json()
            # Get the instance of the recentely created Team object
            db_team = Team.objects.get(id=team_id)
            players = data['squad']
            # For each player found in the response
            for player in players:
                player['team_id'] = db_team.id
                player['date_of_birth'] = player['dateOfBirth']
                player['country_of_birth'] = player['countryOfBirth']
                serializer = PlayerSerializer(data=player)
                serializer.is_valid(serializer)
                serializer.save()
                status = HTTP_201_CREATED
        elif external_request.status_code == 404:
            return HTTP_404_NOT_FOUND
        else:
            # With the retry strategy is pretty unlikey but should be handle
            return HTTP_504_GATEWAY_TIMEOUT
    return status
