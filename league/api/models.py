from django.db import models


class Competition(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=10)
    area = models.CharField(max_length=30)


class Team(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    short_name = models.CharField(max_length=30)
    tla = models.CharField(max_length=10)
    area = models.CharField(max_length=30)
    email = models.EmailField(max_length=120, null=True, blank=True)
    competitions = models.ManyToManyField(Competition, related_name="team_list")


class Player(models.Model):
    name = models.CharField(max_length=50)
    position = models.CharField(max_length=20, null=True, blank=True)
    date_of_birth = models.DateTimeField(null=True, blank=True)
    country_of_birth = models.CharField(max_length=30)
    nationality = models.CharField(max_length=40)
    team_id = models.ForeignKey(
        Team,
        on_delete=models.CASCADE,
    )
