# Core
Django==2.2
djangorestframework==3.11.0
mysqlclient==1.4.6
requests==2.24.0
requests-toolbelt==0.9.1
django-rest-swagger==2.2.0

# Developer Tools
ipdb==0.13.3

# Testing
pytest==4.6.0
pytest-django==3.9.0
coverage==5.1
pytest-cov==2.10.1
requests-mock==1.7.0
